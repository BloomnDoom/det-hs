module Main where
import Data.Function ( (&) )
import Data.List.Split ( chunksOf, splitOn )
import System.Environment (getArgs)

determinant :: Fractional a => [[a]] -> a
determinant [[x]] = x
determinant mat =
  head mat & zip [0..] & map(\x -> snd x * determinant (minor (fst x) mat))
    & zip [0..] & map(\x -> snd x * ((-1)^ fst x)) & sum

minor :: Fractional a => Int -> [[a]] -> [[a]]
minor c mat =
  map ((map snd . filter(\x -> fst x/= c)) . zip [0..]) (drop 1 mat)

strToArr :: [Char] -> [[Float]]
strToArr str =
  map(\x -> read x :: Float) a & chunksOf(truncate $ sqrt l)
  where a = splitOn "," str
        l = fromIntegral $ length a

main :: IO ()
main = do
  args <- getArgs
  if length args /=1 then print "USAGE: R1C1,R1C2...,R2C1,R2C2..."
  else do
    let dim = sqrt $ fromIntegral $ length(splitOn "," (head args))
    if dim == fromIntegral(truncate dim) then do
      print (determinant $ strToArr $ head args)
    else do print "Input must be a square matrix"
